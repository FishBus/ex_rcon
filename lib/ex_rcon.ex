defmodule ExRcon do
  
  def new_connection(host, port, password \\ nil, timeout \\ 5_000) do
    {:ok, pid} = Supervisor.start_child(ExRcon.Supervisor, [%{host: host, port: port, args: %{timeout: timeout}}])
    auth(pid, password)
    pid
  end
  
  def auth(pid, password) when is_binary(password) do
    GenServer.call(pid, {:auth, password})
  end
  def auth(pid, _password), do: pid
  
  def exec(pid, message, call_timeout \\ 5_000) do
    GenServer.call(pid, {:exec, message}, call_timeout)
  end
  
  def set_timeout(pid, timeout) do
    GenServer.call(pid, {:set_timeout, timeout})
  end
  
  def stop(pid) do
    GenServer.stop(pid, :normal)
  end
end
