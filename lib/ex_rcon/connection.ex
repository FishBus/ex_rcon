defmodule ExRcon.Connection do
  
  alias ExRcon.Impl
  
  defstruct(
    socket: nil,
    password: nil,
    trailing_packet: false,
    sequence: 0,
    timeout: 5_000
  )
  
  use GenServer
  
  def start_link(args) do
    GenServer.start_link(__MODULE__, args)
  end
  
  def init(args) do
    IO.inspect args
    {:ok, socket} = Impl.connect(args[:host], args[:port]) |> IO.inspect
    {:ok, %__MODULE__{socket: socket}}
  end
  
  def handle_call({:auth, password}, _from, state) do
    state = auth(state, password)
    {:reply, true, state}
  end
  def handle_call({:exec, message}, _from, state) do
    {state, response} = exec(state, message)
    {:reply, response, state}
  end
  def handle_call({:set_timeout, timeout}, _from, state) when is_integer(timeout) do
    state = %{state | timeout: timeout}
    {:reply, timeout, state}
  end
  
  def terminate(_reason, state) do
    # cleanup (close socket)
    #IO.puts "Terminating: #{inspect self()}: #{inspect reason}"
    Impl.disconnect(state.socket)
  end
  
  defp auth(state, password) do
    {:ok, seq} = Impl.auth(state.socket, state.sequence, password, state.timeout, state.trailing_packet)
    %{state | sequence: seq, password: password}
  end
  
  defp exec(state, message) do
    {:ok, seq, response} = Impl.exec(state.socket, state.sequence, message, state.timeout, state.trailing_packet)
    { %{state | sequence: seq}, response }
  end
  
end
