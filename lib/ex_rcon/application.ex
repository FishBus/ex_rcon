defmodule ExRcon.Application do
  use Application

  def start(_type, _args) do
    import Supervisor.Spec
    
    children = [
      worker(ExRcon.Connection, [], restart: :temporary),
    ]
    
    opts = [
      name: ExRcon.Supervisor,
      strategy: :simple_one_for_one,
    ]
    
    Supervisor.start_link(children, opts)
  end
end
