defmodule ExRcon.Impl do
  @moduledoc """
  Based on https://gist.github.com/evadne/5d4546eea52d2634f2b4d9386f43f2ab
  
  A simple gen_tcp based implementation of a Source RCON client for Factorio
  
  - [Source RCON Protocol](https://developer.valvesoftware.com/wiki/Source_RCON_Protocol)
  
  By calling `connect/2`, you can obtain an open TCP socket,
  which can then be used with `auth/3` or `exec/3`.
  """
  
  @default_timeout 5000
  @packet_out_types %{
    auth: 3, 
    exec: 2, 
    over: -1
  }
  @packet_in_types %{
    0 => :exec_response,
    2 => :auth_response
  }
  @max_read_chunk 1048576
  
  @doc """
  Open a connection to the nominated server. Use raw mode, so the amount of data
  to be read can be controlled precisely.
  """
  @spec connect(binary(), non_neg_integer()) :: {:ok, :gen_tcp.socket} | {:error, any()}
  def connect(host, port, args \\ %{timeout: @default_timeout}) do
    socket_host = to_charlist(host)
    socket_options = [active: false, packet: :raw]
    :gen_tcp.connect(socket_host, port, socket_options, args[:timeout])
  end
  
  def disconnect(socket) do
    :ok = :gen_tcp.shutdown(socket, :write)
    :ok = :gen_tcp.close(socket)
  end
  
  @doc """
  Authenticate with a given RCON password.
  """
  @spec auth(:gen_tcp.socket, non_neg_integer(), non_neg_integer(), String.t) :: {:ok, non_neg_integer()} | {:error, any()}
  def auth(socket, start_seq, password, timeout, trailing_blank \\ false) do
    packets = build_packets(start_seq, :auth, password, trailing_blank)
    with \
      :ok <- send_packets(socket, packets),
      {:ok, {auth_sequence, :auth_response, ""}} <- receive_packet(socket, timeout)
    do
      {:ok, auth_sequence}
    else
      {:error, :closed} -> {:error, :unauthorised}
      {:error, reason} -> {:error, reason}
    end
  end

  @doc """
  Run a RCON command remotely and wait for resopnse. This will send 2 packets,
  so Source RCON commands that have large repsonses can be handled properly. See
  the Source Developer page for details.
  """  
  @spec exec(:gen_tcp.socket, non_neg_integer(), String.t, non_neg_integer(), boolean()) :: {:ok, non_neg_integer(), binary()} | {:error, any()}
  def exec(socket, start_seq, command, timeout, trailing_blank \\ false) do
    packets = build_packets(start_seq, :exec, command, trailing_blank)
    with \
      :ok <- send_packets(socket, packets),
      {:ok, response} <- drain_packets(socket, start_seq + length(packets), timeout)
    do
      {:ok, start_seq + length(packets), response}
    end
  end
  
  
  defp drain_packets(socket, top_sequence, timeout, response \\ <<>>) do
    case receive_packet(socket, timeout) do
      {:ok, {^top_sequence, :exec_response, partial_response}} ->
        {:ok, response <> partial_response}
      {:ok, {_, :exec_response, partial_response}} ->
        drain_packets(socket, top_sequence, timeout, response <> partial_response)
    end
  end
  
  defp build_packets(start_seq, type, body, _trailing_blank=true) do
    [
      build_packet(start_seq+1, type, body),
      build_packet(start_seq+2, :over)
    ]
  end
  defp build_packets(start_seq, type, body, _trailing_blank = false) do
    [
      build_packet(start_seq+1, type, body)
    ]
  end
  
  defp build_packet(sequence, type, body \\ "") do
    <<
      byte_size(body) + 10 :: little-integer-signed-size(32),
      sequence :: little-integer-signed-size(32),
      @packet_out_types[type] :: little-integer-signed-size(32),
      body :: binary,
      0, 0
    >>
  end
  
  defp send_packets(socket, packets) do
    Enum.map(packets, fn p -> :ok = :gen_tcp.send(socket, p) end)
    |> List.last
  end
  
  defp receive_packet(socket, timeout \\ @default_timeout) do
    with \
      {:ok, response_head} <- :gen_tcp.recv(socket, 4, timeout),
      {:ok, response_size} <- parse_response_head(binary_from_response(response_head)),
      {:ok, response_rest} <- receive_response(socket, response_size, timeout),
      {:ok, response} <- parse_response_rest(response_size - 10, binary_from_response(response_rest))
    do
      {:ok, response}
    else
      {:error, :closed} = x -> x 
      x ->
        IO.inspect(x)
        {:error, :invalid_response}
    end
  end
  
  defp receive_response(socket, bytes, timeout) do
    {:ok, res} = receive_response(socket, bytes, timeout, [])
    {:ok, IO.iodata_to_binary(res)}
  end
  defp receive_response(socket, remaining, timeout, chunks) when remaining > @max_read_chunk do
    {:ok, data_pre} = receive_response(socket, remaining - @max_read_chunk, timeout, chunks)
    {:ok, data_post} = :gen_tcp.recv(socket, @max_read_chunk, timeout)
    {:ok, [data_pre | data_post]}
  end
  defp receive_response(socket, remaining, timeout, _data_pre) do
    :gen_tcp.recv(socket, remaining, timeout)
  end
  
  defp parse_response_head(<<size :: little-integer-signed-size(32)>>), do: {:ok, size}
  defp parse_response_head(_), do: {:error, :badarg}
  
  defp parse_response_rest(body_size, data) do
    case data do
      <<
        response_sequence :: little-integer-signed-size(32),
        response_type :: little-integer-signed-size(32),
        response_body :: binary-size(body_size),
        0, 0
      >> ->
        decoded_type = @packet_in_types[response_type]
        {:ok, {response_sequence, decoded_type, response_body}}
      _ ->
        {:error, :badarg}
    end
  end
  
  defp binary_from_response(response) when is_binary(response) do
    response
  end
  defp binary_from_response(response) when is_list(response) do
    :erlang.iolist_to_binary(response)
  end
end
